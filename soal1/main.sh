#!/bin/bash

fun_att(){
	n=0
        awk -v usr=$username '
        BEGIN {print " "}
        $0 ~ "LOGIN" && $0 ~ usr {++n}
        END {print "total hasil hitung",n,"kali"}' $log	
}

fun_unzip(){
        unzip -P $password $folder.zip
        rm $folder.zip

        start=$(find $folder -type f | wc -l)
        fun_start_dl
}

fun_start_dl(){
        n=$(echo "${command//[!0-9]/}" | bc -l)
 
        for(( i=$start+1; i<=$n+$start; i++ ))
        do
                wget https://loremflickr.com/320/240 -O $folder/PIC_$i.jpg
        done

        zip --password $password -r $folder.zip $folder/
        rm -rf $folder
}

fun_dl(){
        if [[ ! -f "$folder.zip" ]]
        then
                mkdir $folder
                start=0
                fun_start_dl
        else
                fun_unzip
        fi

}

fun_loged(){
        echo -n "Masukkan command: "
        read command

        if [[ $command == "dl "@([0-9]) ]] || [[ $command == "dl "@([0-9])@([0-9]) ]]
        then
                fun_dl
        elif [[ $command == att ]]
        then
                fun_att
        else
                echo "Command tidak ditemukan!"
        fi
}

fun_login(){
	if [ ${#password} -lt 8 ]
	then
		echo -e "\n\nPassword minimal 8 karakter!"
		echo $dateTime "LOGIN: ERROR Failed login attempt on user" $username >> $log

	elif [[ "$password" != *[A-Z]* || "$password" != *[a-z]* || "$password" != *[0-9]* ]]

	then
		echo -e "\n\nPassword harus memiliki huruf kapital, huruf kecil, dan angka"
		echo $dateTime "LOGIN: ERROR Failed login attempt on user" $username >> $log

        elif grep -q $password "$user"
	then
		echo $dateTime "LOGIN: INFO User" $username "logged in" >> log.txt
                echo -e "\n\nLogin Berhasil!"
		fun_loged

	else
		echo -e "\n\nPeriksa kembali password anda!"
                echo $dateTime "LOGIN: ERROR Failed login attempt on user" $username >> $log
	fi
}


dateTime=$(date +"%D %T")
user=./users/user.txt
log=./log.txt

echo "--LOGIN--"

echo -n "Username: "
read username

echo -n "Password: "
read -s password

folder=$(date +%Y-%m-%d)_$username

fun_login
