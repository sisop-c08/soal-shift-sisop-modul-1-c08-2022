#!/bin/bash

fun_register() {
	local user=./users/user.txt
	local log=./log.txt

	if grep -q $username "$log"
	then
		echo -e "\n\nUsername telah digunakan!"
		echo $dateTime REGISTER: ERROR User already exists >> $log

	elif [ ${#password} -lt 8 ]
	then
		echo -e "\n\nPassword minimal 8 karakter!"

	elif [[ "$password" != *[A-Z]* || "$password" != *[a-z]* || "$password" != *[0-9]* ]]
	then
		echo -e "\n\nPassword harus memiliki huruf kapital, huruf kecil, dan angka"

        elif [ $password = $username ]
        then
                echo -e "\n\nPassword tidak boleh sama dengan username!"

    	else
      		echo -e "\n\nRegister Berhasil!"
      		echo $dateTime REGISTER: INFO User $username registered successfully >> $log
		echo $username $password >> $user
	fi
}


dateTime=$(date +"%D %T")

echo "--REGISTER--"

echo -n "Username: "
read username

echo -n "Password: "
read -s password

fun_register
