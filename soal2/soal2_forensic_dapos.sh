#!/bin/bash

mkdir forensic_log_website_daffainfo_log

# Rata-rata request per jam
awk -F: '
NR == 1 { min = max = $3 }
{ if ($3 > max) max = $3; else if ($3 < min) min = $3 }
END {
printf ("Rata-rata serangan adalah sebanyak %2.0f request per jam.\n", (NR-1)/(max-min))
}' log_website_daffainfo.log > forensic_log_website_daffainfo_log/ratarata.txt

# IP yang paling banyak melakukan request ke server 
awk -F: '
NR>1 { gsub(/"/, "") } array[$1]++ >= max { max = array[$1] } 
END { for (ip in array) if(max == array[ip]) print "IP yang paling banyak mengakses server adalah:", ip, "sebanyak", array[ip], "requests\n" } 
' log_website_daffainfo.log > forensic_log_website_daffainfo_log/result.txt

# Jumlah request menggunakan curl sebagai user agent
awk -F: '
NR>1 && $9 ~ /curl/ { ++n } 
END { print "Ada", n, "requests yang menggunakan curl sebagai user-agent\n" }
' log_website_daffainfo.log >> forensic_log_website_daffainfo_log/result.txt

# Daftar IP yang mengakses website pada Jam 2 tanggal 22
awk -F: ' 
NR>1 { gsub(/"/, "") } $2 ~ /^22/ && $3 ~ /^02$/ { array[$1]++ }
END { for (ip in array) print ip, "Jam 2 pagi" }
' log_website_daffainfo.log >> forensic_log_website_daffainfo_log/result.txt

