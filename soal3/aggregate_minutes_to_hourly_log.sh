#!bin/bash

fun_average(){
	echo -n "average," >> $hourLog

	for(( i=1; i<=11; i++ ))
	do
        	if [ $i != 10 ]
        	then
                	awk -F ",|M" -v x=$i 'BEGIN {sum=0; count=0}{sum=sum+$x; count=count+1} END {printf sum/count}' $hourLogTemp | sed 's/,/./' >> $hourLog
        	else
                	awk -F "," -v x=$i 'NR==1 {printf $x}' $hourLogTemp >> $hourLog
        	fi
        	fun_print_sep
	done	
}

fun_max(){
	echo -n "maximum," >> $hourLog

	for(( i=1; i<=11; i++ ))
        do
		if [ $i != 10 ]
		then
			awk -F ",|M" -v x=$i '{if ($x > max) max=$x}END{printf max}' $hourLogTemp >> $hourLog
		else
                        awk -F "," -v x=$i 'NR==1 {printf $x}' $hourLogTemp >> $hourLog
		fi
		
		fun_print_sep
	done
}

fun_min(){
	echo -n "minimum," >> $hourLog

	for(( i=1; i<=11; i++ ))
	do
		if [ $i != 10 ]
                then
			awk -F ",|M" -v x=$i 'BEGIN{min=9999999}{if($x<min) min=$x}END{printf min}' $hourLogTemp >> $hourLog
		else
                        awk -F "," -v x=$i 'NR==1 {printf $x}' $hourLogTemp >> $hourLog
                fi
                
		fun_print_sep
	done
}

fun_print_sep(){
	if [ $i != 11 ]
  	then
                echo -n "," >> $hourLog
       	else
                echo "M" >> $hourLog
      	fi
}

fun_agregasi(){
	echo $metDesc > $hourLog
	fun_min
	fun_max
	fun_average
	chmod 600 $hourLog
	rm $hourLogTemp
}

metDesc="type,mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size"
path=/home/dhika/
pathLog=$path"log/"
date=$(date +"%Y%m%d")
hour=$(date +"%H")
minuteLog=$pathLog"metrics_"$date$(($hour-1))"*.log"
hourLogTemp=$pathLog"metrics_agg_"$date$(($hour-1))".tmp"
hourLog=$pathLog"metrics_agg_"$date$(($hour-1))".log"

awk 'NR%2 == 0 {print}' $minuteLog > $hourLogTemp

fun_agregasi
