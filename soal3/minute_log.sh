#!bin/bash

fun_minute_log(){
	free -m | grep -Eo '[0-9]+' > $minuteLogTemp
	du -sh $path | awk '{print $2"\n"$1}' >> $minuteLogTemp
	awk -v md=$metDesc 'BEGIN {print md} {printf "%s%s",sep,$1; sep=","} END{print ""}' $minuteLogTemp > $minuteLog
	chmod 600 $minuteLog
	rm $minuteLogTemp

}

metDesc="mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size" 
dateTime=$(date +"%Y%m%d%H%M%S")
path=/home/dhika/
pathLog=$path"log"
minuteLog=$pathLog"/metrics_"$dateTime".log"
minuteLogTemp="metrics_"$dateTime".tmp"

fun_minute_log
