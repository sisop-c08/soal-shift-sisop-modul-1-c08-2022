# Laporan Praktikum Modul 1
# Identitas Kelompok
### Kelompok C08
 1.  __Doanda Dresta Rahma	5025201049__
 2.  __Putu Andhika Pratama	5025201132__
 3.  __Muhammad Raihan Athallah	5025201206__

# Daftar Isi
- [Soal 1](#soal-1)
  - [Membuat Register dan Login](#a-membuat-sistem-register-dan-login)
  - [Ketentuan Password Akun](#b-ketentuan-password-akun)
  - [Pencatatan Aktivitas Login](#c-pencatatan-aktivitas-login)
  - [2 Command](#d-2-command)
  - [Source Code](#source-code-soal-1)
- [Soal 2](#soal-2)
  - [Membuat folder forensic_log_website_daffainfo_log](#a-membuat-folder-forensic_log_website_daffainfo_log)
  - [Rata-Rata Request per Jam](#b-rata-rata-request-per-jam)
  - [Menampilkan IP Request Terbanyak](#c-menampilkan-ip-request-terbanyak)
  - [Total User-agent Curl](#d-total-user-agent-curl)
  - [Daftar IP yang Mengakses Website Pada Jam 2 Tanggal 22](#e-daftar-ip-yang-mengakses-website-pada-jam-2-tanggal-22)
  - [Source Code](#source-code-soal-2)
- [Soal 3](#soal-3)
  - [Masukkan Semua Metrics ke Dalam Suatu File](#a-masukkan-semua-metrics-ke-dalam-suatu-file)
  - [Script Untuk Mencatat Metrics Berjalan Otomatis Setiap Menit](#b-script-untuk-mencatat-metrics-berjalan-otomatis-setiap-menit)
  - [Buat Satu Script Untuk Membuat Agregasi File Log ke Satuan Jam](#c-buat-satu-script-untuk-membuat-agregasi-file-log-ke-satuan-jam)
  - [Semua File log Hanya Dapat Dibaca Oleh User Pemilik File](#d-semua-file-log-hanya-dapat-dibaca-oleh-user-pemilik-file)
  - [Source Code](#source-code-soal-3)


# Soal 1

## a. Membuat Sistem Register dan Login

Diminta untuk membuat sistem register pada script register.sh dan setiap user yang berhasil
didaftarkan disimpan di dalam file ./users/user.txt juga membuat sistem login
yang dibuat di script main.sh. 

```bash
#create register.sh file
$ > register.sh

#create a users folder directory
$ mkdir users

# move to users directory
$ cd users

#create user.txt file
$ > user.txt
```



## b. Ketentuan Password Akun

Saat membuat akun di sistem register user harus mengikuti aturan untuk membuat password yaitu:

```bash
fun_register() {
	local user=./users/user.txt
	local log=./log.txt

	if grep -q $username "$log"
	then
		echo -e "\n\nUsername telah digunakan!"
		echo $dateTime REGISTER: ERROR User already exists >> $log

	elif [ ${#password} -lt 8 ]
	then
		echo -e "\n\nPassword minimal 8 karakter!"

	elif [[ "$password" != *[A-Z]* || "$password" != *[a-z]* || "$password" != *[0-9]* ]]
	then
		echo -e "\n\nPassword harus memiliki huruf kapital, huruf kecil, dan angka"

        elif [ $password = $username ]
        then
                echo -e "\n\nPassword tidak boleh sama dengan username!"

    	else
      		echo -e "\n\nRegister Berhasil!"
      		echo $dateTime REGISTER: INFO User $username registered successfully >> $log
		echo $username $password >> $user
	fi
}
```

- __Minimal 8 karakter__



Menggunakan if/else dengan kondisi dimana password harus memiliki minimal 8 karakter menggunakan __-lt__ untuk pembanding yang berarti "<" (kurang dari) 

```bash
elif [ ${#password} -lt 8 ]
	then
		echo -e "\n\nPassword minimal 8 karakter!"
```
![minimal 8 karakter](img/1_kurang_dari_8.jpg)

- __Memiliki minimal 1 huruf kapital dan 1 huruf kecil__

Pada if/else dengan kondisi dimana password harus memiliki minimal 1 huruf kapital dan 1 huruf kecil maka dengan dibuat password tidak boleh hanya menggunakan huruf kapital [A-Z] atau password tidak boleh hanya menggunakan hurus kecil [a-z] atau password hanya berupa angka [0-9]

```bash
elif [[ "$password" != *[A-Z]* || "$password" != *[a-z]* || "$password" != *[0-9]* ]]
	then
		echo -e "\n\nPassword harus memiliki huruf kapital, huruf kecil, dan angka"
```
![1 kapital 1 kecil](img/1_tanpa_alphanumeric.jpg)

- __Alphanumeric__

sama seperti if/else sebelumnya alphanumeric disini maksutnya password harus terdiri dari gabungan alfabet dan angka dalam satu password

```bash
elif [[ "$password" != *[A-Z]* || "$password" != *[a-z]* || "$password" != *[0-9]* ]]
	then
		echo -e "\n\nPassword harus memiliki huruf kapital, huruf kecil, dan angka"
```
![1 kapital 1 kecil](img/1_tanpa_alphanumeric.jpg)

- __Tidak boleh sama dengan username__

Menggunakan __grep__ untuk melakukan pengecekkan dalam file log apakah terdapat kesamaan antara username dengan password log 

```bash
elif [ $password = $username ]
        then
                echo -e "\n\nPassword tidak boleh sama dengan username!"

```
![sama dengan username](img/1_sama_dengan_id.jpg)

## c. Pencatatan Aktivitas Login


Setiap percobaan login dan register akan tercatat pada log.txt dengan 
format :MM/DD/YY hh:mm:ss MESSAGE. Message pada log akan berbeda tergantung aksi
yang dilakukan user

```bash
fun_login(){
	if [ ${#password} -lt 8 ]
	then
		echo -e "\n\nPassword minimal 8 karakter!"
		echo $dateTime "LOGIN: ERROR Failed login attempt on user" $username >> $log

	elif [[ "$password" != *[A-Z]* || "$password" != *[a-z]* || "$password" != *[0-9]* ]]

	then
		echo -e "\n\nPassword harus memiliki huruf kapital, huruf kecil, dan angka"
		echo $dateTime "LOGIN: ERROR Failed login attempt on user" $username >> $log

        elif grep -q $password "$user"
	then
		echo $dateTime "LOGIN: INFO User" $username "logged in" >> log.txt
                echo -e "\n\nLogin Berhasil!"
		fun_loged

	else
		echo -e "\n\nPeriksa kembali password anda!"
                echo $dateTime "LOGIN: ERROR Failed login attempt on user" $username >> $log
	fi
}
```

- __Ketika register dengan username yang sudah terdaftar, maka
  message pada log adalah REGISTER: ERROR User already exists__



Menggunakan __grep__ untuk mengecek pada file log.txt apakah terdapat username yang sama dengan yang diinputkan oleh user. Bila ditemukan kesamaan username maka akan muncul kalimat "Username telah digunakan" dan mencatat tanggal dan aktivitas tersebut dalam file log.txt dengan pesan "REGISTER: ERROR User already exists"

```bash
if grep -q $username "$log"
	then
		echo -e "\n\nUsername telah digunakan!"
		echo $dateTime REGISTER: ERROR User already exists >> $log
```
![sudah terdaftar](img/1_username_sdh_ada.jpg)
![sudah terdaftar(log)](img/1_sudah_terdaftar.jpg)

- __Ketika percobaan register berhasil, maka message pada log adalah REGISTER:
  INFO User USERNAME registered successfully__

```bash
        else
      		echo -e "\n\nRegister Berhasil!"
      		echo $dateTime REGISTER: INFO User $username registered successfully >> $log
```
![password sesuai syarat](img/1_sesuai_syarat.jpg)
![register berhasil(log)](img/1_sudah_terdaftar.jpg)

- __Ketika user mencoba login namun passwordnya salah, maka message pada
  log adalah LOGIN: ERROR Failed login attempt on user USERNAME__

Menggunakan __grep__ untuk melakukan pengecekkan pada file user.txt apakah password dan username sesuai apa tidak jika sesuai akan mencatatkan aktivitas di log dengan pesan  __"LOGIN: INFO User" $username "logged in"__ dan menjalankan fungsi __fun_loged__. Bila password salah maka akan mencatat di log dengan pesan __"LOGIN: ERROR Failed login attempt on user"__

```bash
elif grep -q $password "$user"
	then
		echo $dateTime "LOGIN: INFO User" $username "logged in" >> log.txt
                echo -e "\n\nLogin Berhasil!"
		fun_loged

	else
		echo -e "\n\nPeriksa kembali password anda!"
                echo $dateTime "LOGIN: ERROR Failed login attempt on user" $username >> $log
```
![password salah](img/1_password_salah.jpg)
![password salah(log)](img/1_error_attemp.jpg)

- __Ketika user berhasil login, maka message pada log adalah LOGIN: INFO User
  USERNAME logged in__

```bash
elif grep -q $password "$user"
	then
		echo $dateTime "LOGIN: INFO User" $username "logged in" >> log.txt
                echo -e "\n\nLogin Berhasil!"
		fun_loged
```
 ![login berhasil](img/1_login_berhasil.jpg)
 ![login berhasil(log)](img/1_logged_in.jpg)

## d. 2 Command


Fungsi logged akan berjalan setelah user berhasil login kemudian user akan menginput command untuk menjalankan fungsi lainnya untuk command __dl__ akan menjalakan fungsi dl dan untuk comman __att__ akan menjalankan fungsi att

```bash
fun_loged(){
        echo -n "Masukkan command: "
        read command

        if [[ $command == "dl "@([0-9]) ]] || [[ $command == "dl "@([0-9])@([0-9]) ]]
        then
                fun_dl
        elif [[ $command == att ]]
        then
                fun_att
        else
                echo "Command tidak ditemukan!"
        fi
}
```

- __dl N__ 

( N = Jumlah gambar yang akan didownload)
Untuk mendownload gambar dari https://loremflickr.com/320/240 dengan
jumlah sesuai dengan yang diinputkan oleh user. Hasil download akan
dimasukkan ke dalam folder dengan format nama
YYYY-MM-DD_USERNAME. Gambar-gambar yang didownload juga memiliki
format nama PIC_XX, dengan nomor yang berurutan (contoh : PIC_01,
PIC_02, dst. ). Setelah berhasil didownload semua, folder akan otomatis di
zip dengan format nama yang sama dengan folder dan dipassword sesuai
dengan password user tersebut. Apabila sudah terdapat file zip dengan
nama yang sama, maka file zip yang sudah ada di unzip terlebih dahulu,
barulah mulai ditambahkan gambar yang baru, kemudian folder di zip
kembali dengan password sesuai dengan user.

### Fungsi dl

fungsi ini bertugas untuk membuat folder dengan format nama YYYY-MM-DD_USERNAME jika sebelumnya belum ada folder dangan nama yang sama sesuai tanggal jika sudah ada maka akan langsung memanggil fungsi unzip 

```bash 
fun_dl(){
        if [[ ! -f "$folder.zip" ]]
        then
                mkdir $folder
                start=0
                fun_start_dl
        else
                fun_unzip
        fi

}
```

### Fungsi Unzip

Baris pertama dalam fungsi bertujuan untuk unzip file sesuai dengan nama, kemudian pada baris kedua menghapus file zip tersebut.Pada baris keempat fungsi unzip akan mencari di dalam folder ( __$folder__ ). wc -l berfungsi untuk menghitung berapa jumlah file di dalam folder setelah itu jumlah folder akan diassign ke dalam variabel start yang berguna untuk melakukan looping pada fungsi dl setelahnya 

```bash 
fun_unzip(){
        unzip -P $password $folder.zip
        rm $folder.zip

        start=$(find $folder -type f | wc -l)
        fun_start_dl
}
```
### Fungsi start_dl

Pada fungsi start_dl di baris pertama akan mengambil angka dari input user kemudian pada baris ketiga akan menjalaan looping untuk mendownload gambar. Setelah download dilakukan output(-O) akan dimasukkan ke dalam __folder/namafile__. Kemudian pada baris ke-8 berfungsi untuk meng-zip semua file di dalam __$folder__ kemudian nama __$folder.zip__ di isi dnegan password sesuai dengan __$password__

```bash 
fun_start_dl(){
        n=$(echo "${command//[!0-9]/}" | bc -l)
 
        for(( i=$start+1; i<=$n+$start; i++ ))
        do
                wget https://loremflickr.com/320/240 -O $folder/PIC_$i.jpg
        done

        zip --password $password -r $folder.zip $folder/
        rm -rf $folder
}
```
#### command dl 3
![command dl 3](img/1_command_dl3.jpg)
#### file zip setelah command dl 3
![file zip setelah dl 3](img/1_file_zip.jpg)
#### command dl 2
![command dl 2](img/1_command_dl2.jpg)
#### file zip setelah command dl 2
![file zip setelah dl 2](img/1_file_zip2.jpg)






- __att__ 

Comamnd ini unutk menghitung jumlah percobaan login baik yang berhasil maupun tidak dari user yang sedang login saat ini. Menggunakan __awk__ meng assign variabel __usr__ dengan username pengguna kemudian di dalamnya terdapat syarat yang harus ada sebelum menjalankan perintah __++n__ yaitu dalam file log terdapat line yang mengandung kata __LOGIN__ dan terdapat username pengguna apabila kedua syarat terpenuhi maka __n__ akan bertambah. Saat sudah tidak ada lagi line yang memenuhi syarat __awk__ akan berhenti dan menampilkan total percobaan login pengguna.
```bash 
fun_att(){
	n=0
        awk -v usr=$username '
        BEGIN {print " "}
        $0 ~ "LOGIN" && $0 ~ usr {++n}
        END {print "total hasil hitung",n,"kali"}' $log	
}
```
![command att](img/1_command_att.jpg)

## Source Code Soal 1
### register.sh

```bash
#!/bin/bash

fun_register() {
	local user=./users/user.txt
	local log=./log.txt

	if grep -q $username "$log"
	then
		echo -e "\n\nUsername telah digunakan!"
		echo $dateTime REGISTER: ERROR User already exists >> $log

	elif [ ${#password} -lt 8 ]
	then
		echo -e "\n\nPassword minimal 8 karakter!"

	elif [[ "$password" != *[A-Z]* || "$password" != *[a-z]* || "$password" != *[0-9]* ]]
	then
		echo -e "\n\nPassword harus memiliki huruf kapital, huruf kecil, dan angka"

        elif [ $password = $username ]
        then
                echo -e "\n\nPassword tidak boleh sama dengan username!"

    	else
      		echo -e "\n\nRegister Berhasil!"
      		echo $dateTime REGISTER: INFO User $username registered successfully >> $log
		echo $username $password >> $user
	fi
}


dateTime=$(date +"%D %T")

echo "--REGISTER--"

echo -n "Username: "
read username

echo -n "Password: "
read -s password

fun_register

```
#### main.sh
```bash
#!/bin/bash

fun_att(){
	n=0
        awk -v usr=$username '
        BEGIN {print " "}
        $0 ~ "LOGIN" && $0 ~ usr {++n}
        END {print "total hasil hitung",n,"kali"}' $log	
}

fun_unzip(){
        unzip -P $password $folder.zip
        rm $folder.zip

        start=$(find $folder -type f | wc -l)
        fun_start_dl
}

fun_start_dl(){
        n=$(echo "${command//[!0-9]/}" | bc -l)
 
        for(( i=$start+1; i<=$n+$start; i++ ))
        do
                wget https://loremflickr.com/320/240 -O $folder/PIC_$i.jpg
        done

        zip --password $password -r $folder.zip $folder/
        rm -rf $folder
}

fun_dl(){
        if [[ ! -f "$folder.zip" ]]
        then
                mkdir $folder
                start=0
                fun_start_dl
        else
                fun_unzip
        fi

}

fun_loged(){
        echo -n "Masukkan command: "
        read command

        if [[ $command == "dl "@([0-9]) ]] || [[ $command == "dl "@([0-9])@([0-9]) ]]
        then
                fun_dl
        elif [[ $command == att ]]
        then
                fun_att
        else
                echo "Command tidak ditemukan!"
        fi
}

fun_login(){
	if [ ${#password} -lt 8 ]
	then
		echo -e "\n\nPassword minimal 8 karakter!"
		echo $dateTime "LOGIN: ERROR Failed login attempt on user" $username >> $log

	elif [[ "$password" != *[A-Z]* || "$password" != *[a-z]* || "$password" != *[0-9]* ]]

	then
		echo -e "\n\nPassword harus memiliki huruf kapital, huruf kecil, dan angka"
		echo $dateTime "LOGIN: ERROR Failed login attempt on user" $username >> $log

        elif grep -q $password "$user"
	then
		echo $dateTime "LOGIN: INFO User" $username "logged in" >> log.txt
                echo -e "\n\nLogin Berhasil!"
		fun_loged

	else
		echo -e "\n\nPeriksa kembali password anda!"
                echo $dateTime "LOGIN: ERROR Failed login attempt on user" $username >> $log
	fi
}


dateTime=$(date +"%D %T")
user=./users/user.txt
log=./log.txt

echo "--LOGIN--"

echo -n "Username: "
read username

echo -n "Password: "
read -s password

folder=$(date +%Y-%m-%d)_$username

fun_login


```


# Soal 2

## a. Membuat folder forensic_log_website_daffainfo_log.

Membuat folder forensic_log_website_daffainfo_log dengan command ini pada terminal 
```bash
mkdir forensic_log_website_daffainfo_log
```

## b. Rata-Rata Request per Jam

Dikarenakan serangan yang diluncurkan ke website https://daffa.info sangat banyak,
Dapos ingin tahu berapa rata-rata request per jam yang dikirimkan penyerang ke
website. Kemudian masukkan jumlah rata-ratanya ke dalam sebuah file bernama
ratarata.txt ke dalam folder yang sudah dibuat sebelumnya.  
  
Pertama diawali dengan `awk -F:`  dimana `-F:` menjadikan titik dua sebagai separator variable. Kemudian melakukan inisialisasi jam (var $3) minimal dan maksimal pada baris pertama `NR == 1 { min = max = $3 }`. Kemudian untuk setiap baris mengupdate nilai max dan min `{ if ($3 > max) max = $3; else if ($3 < min) min = $3 }`. Kemudian rata-rata dihitung dengan membagi jumlah baris (kecuali baris pertama) dengan span dari jam ditambah 1 `(NR-1)/(max-min+1)`.

```bash
# Rata-rata request per jam
awk -F: '
NR == 1 { min = max = $3 }
{ if ($3 > max) max = $3; else if ($3 < min) min = $3 }
END {
printf ("Rata-rata serangan adalah sebanyak %2.0f request per jam.\n", (NR-1)/(max-min))
}' log_website_daffainfo.log > forensic_log_website_daffainfo_log/ratarata.txt

```

## c. Menampilkan IP Request Terbanyak 

Sepertinya penyerang ini menggunakan banyak IP saat melakukan serangan ke
website https://daffa.info, Dapos ingin menampilkan IP yang paling banyak
melakukan request ke server dan tampilkan berapa banyak request yang dikirimkan
dengan IP tersebut. Masukkan outputnya kedalam file baru bernama result.txt
kedalam folder yang sudah dibuat sebelumnya.  
  
Untuk mencari ip yang paling banyak melakukan request perlu membuat array/ key-value pair map dengan IP (var $1) sebagai key dan banyaknya request sebagai value (yang di increment setiap baris). Selain itu dicari nilai value yang maksimum `array[$1]++ >= max { max = array[$1] }`. 
Pada akhir pembacaan file akan dilakukan traversal terhadap map yang dibuat dan mengoutputkan IP dan frekuensi requestnya `END { for (ip in array) if(max == array[ip]) print`. Fungsi `gsub` digunakan untuk menghilangkan tanda kutip (").
```bash
# IP yang paling banyak melakukan request ke server 
awk -F: '
NR>1 { gsub(/"/, "") } array[$1]++ >= max { max = array[$1] } 
END { for (ip in array) if(max == array[ip]) print "IP yang paling banyak mengakses server adalah:", ip, "sebanyak", array[ip], "requests\n" } 
' log_website_daffainfo.log >> forensic_log_website_daffainfo_log/result.t

```

## d. Total User-agent Curl 

Beberapa request ada yang menggunakan user-agent ada yang tidak. Dari banyaknya
request, berapa banyak requests yang menggunakan user-agent curl?
Kemudian masukkan berapa banyak requestnya kedalam file bernama result.txt yang
telah dibuat sebelumnya.  
  
Jumlah user-agent curl `n` dapat dicari dengan melakukan increment pada `n` pada setiap baris yang memiliki user-agent (var $9) yang mengandung pola 'curl' `$9 ~ /curl/ { ++n }`.
```bash
# Jumlah request menggunakan curl sebagai user agent
awk -F: '
NR>1 && $9 ~ /curl/ { ++n } 
END { print "Ada", n, "requests yang menggunakan curl sebagai user-agent\n" }
' log_website_daffainfo.log >> forensic_log_website_daffainfo_log/result.txt

```


## e. Daftar IP yang Mengakses Website Pada Jam 2 Tanggal 22 

Pada jam 2 pagi pada tanggal 23 terdapat serangan pada website, Dapos ingin
mencari tahu daftar IP yang mengakses website pada jam tersebut. Kemudian
masukkan daftar IP tersebut kedalam file bernama result.txt yang telah dibuat
sebelumnya.  
  
Daftar IP dapat dicari dengan memasukkan IP (var $1) kedalam array jika tanggal (var $2) diawali '22' dan jam (var $3) adalah '02' `$2 ~ /^22/ && $3 ~ /^02$/ { array[$1]++ }`
```bash
# Daftar IP yang mengakses website pada Jam 2 tanggal 22
awk -F: ' 
NR>1 { gsub(/"/, "") } $2 ~ /^22/ && $3 ~ /^02$/ { array[$1]++ }
END { for (ip in array) print ip, "Jam 2 pagi" }
' log_website_daffainfo.log >> forensic_log_website_daffainfo_log/result.txt

```
## Hasil Soal 2
![hasil soal 2](img/2_hasil_run.jpg)

## Source Code Soal 2
### soal2_forensic_dapos.sh
```bash
#!/bin/bash

mkdir forensic_log_website_daffainfo_log

# Rata-rata request per jam
awk -F: '
NR == 1 { min = max = $3 }
{ if ($3 > max) max = $3; else if ($3 < min) min = $3 }
END {print "Rata-rata serangan adalah sebanyak", (NR-1)/(max-min+1), "request per jam\n"} 
' log_website_daffainfo.log >> forensic_log_website_daffainfo_log/ratarata.txt

# IP yang paling banyak melakukan request ke server 
awk -F: '
NR>1 { gsub(/"/, "") } array[$1]++ >= max { max = array[$1] } 
END { for (ip in array) if(max == array[ip]) print "IP yang paling banyak mengakses server adalah:", ip, "sebanyak", array[ip], "requests\n" } 
' log_website_daffainfo.log >> forensic_log_website_daffainfo_log/result.txt

# Jumlah request menggunakan curl sebagai user agent
awk -F: '
NR>1 && $9 ~ /curl/ { ++n } 
END { print "Ada", n, "requests yang menggunakan curl sebagai user-agent\n" }
' log_website_daffainfo.log >> forensic_log_website_daffainfo_log/result.txt

# Daftar IP yang mengakses website pada Jam 2 tanggal 22
awk -F: ' 
NR>1 { gsub(/"/, "") } $2 ~ /^22/ && $3 ~ /^02$/ { array[$1]++ }
END { for (ip in array) print ip, "Jam 2 pagi" }
' log_website_daffainfo.log >> forensic_log_website_daffainfo_log/result.txt
```







# Soal 3

Buat Program monitoring resource pada komputer kalian. Cukup monitoring ram dan
monitoring size suatu directory. Untuk ram gunakan command `free -m`. Untuk disk gunakan
command `du -sh <target_path>`. Catat semua metrics yang didapatkan dari hasil `free -m`.
Untuk hasil `du -sh <target_path>` catat size dari path directory tersebut. Untuk target_path
yang akan dimonitor adalah /home/{user}/.

## a. Masukkan Semua Metrics ke Dalam Suatu File 

Masukkan semua metrics ke dalam suatu file log bernama metrics_{YmdHms}.log.
{YmdHms} adalah waktu disaat file script bash kalian dijalankan. Misal dijalankan
pada 2022-01-31 15:00:00, maka file log yang akan tergenerate adalah
metrics_20220131150000.log.

```bash
fun_minute_log(){
	free -m | grep -Eo '[0-9]+' > $minuteLogTemp
	du -sh $path | awk '{print $2"\n"$1}' >> $minuteLogTemp
	awk -v md=$metDesc 'BEGIN {print md} {printf "%s%s",sep,$1; sep=","} END{print ""}' $minuteLogTemp > $minuteLog
	...
	rm $minuteLogTemp
}
```

Pada fungsi `fun_minute_log` diawali dengan monitoring ram `free -m` kemudian menggunakan `grep -Eo '[0-9]+'`
untuk mengambil hanya angkanya saja dan diinputkan pada file sementara yaitu `$minuteLogTemp`.
Lalu monitoring size pada `$path` dengan `du -sh $path`, dengan awk mengambil hasilnya dijadikan
perbaris dan diinputkan pada `$minuteLogTemp`.
Kemudian menggunakan awk untuk memasukkan isi dari `$minuteLogTemp` ke file `$minuteLog` sesuai dengan
catatan pada soal
`awk -v md=$metDesc 'BEGIN {print md} {printf "%s%s",sep,$1; sep=","} END{print ""}' $minuteLogTemp > $minuteLog`
Terakhir file `$minuteLogTemp` dihapus `rm $minuteLogTemp`

## b. Script Untuk Mencatat Metrics Berjalan Otomatis Setiap Menit

Dengan membuat cron job pada crontab dengan menjalankan perintah `crontab -e` pada command prompt.
```bash
* * * * * bash /home/dhika/sisop/m1/no3/minute_log.sh
```
![crontab minutely](img/3_crontab.jpg)

Maka perintah `bash /home/dhika/sisop/m1/no3/minute_log.sh` akan berjalan tiap menit

![log metric minutely](img/3_log_metric_minute.jpg)

## c. Buat Satu Script Untuk Membuat Agregasi File Log ke Satuan Jam

Script agregasi akan memiliki info dari file-file yang tergenerate tiap menit. Dalam hasil file
agregasi tersebut, terdapat nilai minimum, maximum, dan rata-rata dari tiap-tiap
metrics. File agregasi akan ditrigger untuk dijalankan setiap jam secara otomatis.
Berikut contoh nama file hasil agregasi metrics_agg_2022013115.log dengan format
metrics_agg_{YmdH}.log

- #### Fungsi Mencari Nilai Rata-Rata
```bash
fun_average(){
	echo -n "average," >> $hourLog

	for(( i=1; i<=11; i++ ))
	do
        	if [ $i != 10 ]
        	then
                	awk -F ",|M" -v x=$i 'BEGIN {sum=0; count=0}{sum=sum+$x; count=count+1} END {printf sum/count}' $hourLogTemp | sed 's/,/./' >> $hourLog
        	else
                	awk -F "," -v x=$i 'NR==1 {printf $x}' $hourLogTemp >> $hourLog
        	fi
        	fun_print_sep
	done	
}
```
Dengan menggunakan for loop sebanyak jumlah kolom `( i=1; i<=11; i++ )` untuk mencari nilai rata-rata
dari tiap kolom menggunakan awk
   - Jika bukan kolom 10
      - awk diinput dari file `$hourLogTemp`
      - menggunakan separator koma atau M untuk tiap kolom `-F ",|M"`
      - menggunakan variabel `-v x=$i`
      - nilai pada tiap baris di kolom x dijumlahkan `sum=sum+$x`
      - menghitung jumlah data dengan `count=count+1`
      - lalu `print sum/count` untuk mendapat rata-rata
      - `sed 's/,/./'` jika hasilnya pecahan (,) akan diganti dengan (.)
      - lalu hasilnya akan dimasukkan ke file `$hourLog`
   - Jika kolom 10
      - Karena nilainya sama dan berupa path maka akan langsung dioutpukan nilainya

Terakhir memanggil `fun_print_sep` untuk mencetak seperator (,)
```bash
fun_print_sep(){
	if [ $i != 11 ]
  	then
                echo -n "," >> $hourLog
       	else
                echo "M" >> $hourLog
      	fi
}
```
jika bukan kolom 11 akan mencetak (,) sedangakn jika kolom 11 akan mencetak (M)

- #### Fungsi Mencari Nilai Maksimum
```bash
fun_max(){
	echo -n "maximum," >> $hourLog

	for(( i=1; i<=11; i++ ))
        do
		if [ $i != 10 ]
		then
			awk -F ",|M" -v x=$i '{if ($x > max) max=$x}END{printf max}' $hourLogTemp >> $hourLog
		else
                        awk -F "," -v x=$i 'NR==1 {printf $x}' $hourLogTemp >> $hourLog
		fi
		
		fun_print_sep
	done
}
```
Hampir sama dengan fungsi rata-rata yang sebelumnya yang membedakan pada awk jika bukan kolom 10
   * jika `$x > max` maka `max=$x`
   * kemudian `printf max` pada `END`
 
- #### Fungsi Mencari Nilai Minimum
```bash
fun_min(){
	echo -n "minimum," >> $hourLog

	for(( i=1; i<=11; i++ ))
	do
		if [ $i != 10 ]
                then
			awk -F ",|M" -v x=$i 'BEGIN{min=9999999}{if($x<min) min=$x}END{printf min}' $hourLogTemp >> $hourLog
		else
                        awk -F "," -v x=$i 'NR==1 {printf $x}' $hourLogTemp >> $hourLog
                fi
                
		fun_print_sep
	done
}
```
Hampir sama juga dengan fungsi rata-rata dan maksimum sebelumnya yang membedakan pada awk jika bukan kolom 10
   - asumsi awal nilai `min=9999999` pada `BEGIN`
   - jika `$x<min` maka `min=$x`
   - kemudian `printf min` pada `END`


Kemudian agar script berjalan tiap jam maka pada file crontab
```bash
0 * * * * bash /home/dhika/sisop/m1/no3/aggregate_minutes_to_hourly_log.sh
```
![crontab hourly](img/3_crontab.jpg)
![log metric hourly](img/3_log_metric_hour.jpg)

## d. semua file log hanya dapat dibaca oleh user pemilik file
Menggunakan `chmod 600` agar file permission menjadi `-rw-------` dimana hanya user yang dapat membaca dan menulisnya

![permssion user](img/3_permission.jpg)

## Source Code Soal 3
#### minute_log.sh 
```bash
#!bin/bash

fun_minute_log(){
	free -m | grep -Eo '[0-9]+' > $minuteLogTemp
	du -sh $path | awk '{print $2"\n"$1}' >> $minuteLogTemp
	awk -v md=$metDesc 'BEGIN {print md} {printf "%s%s",sep,$1; sep=","} END{print ""}' $minuteLogTemp > $minuteLog
	chmod 600 $minuteLog
	rm $minuteLogTemp

}

metDesc="mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size" 
dateTime=$(date +"%Y%m%d%H%M%S")
path=/home/dhika/
pathLog=$path"log"
minuteLog=$pathLog"/metrics_"$dateTime".log"
minuteLogTemp="metrics_"$dateTime".tmp"

fun_minute_log
```
#### aggregate_minutes_to_hourly_log.sh
```bash
#!bin/bash

fun_average(){
	echo -n "average," >> $hourLog

	for(( i=1; i<=11; i++ ))
	do
        	if [ $i != 10 ]
        	then
                	awk -F ",|M" -v x=$i 'BEGIN {sum=0; count=0}{sum=sum+$x; count=count+1} END {printf sum/count}' $hourLogTemp | sed 's/,/./' >> $hourLog
        	else
                	awk -F "," -v x=$i 'NR==1 {printf $x}' $hourLogTemp >> $hourLog
        	fi
        	fun_print_sep
	done	
}

fun_max(){
	echo -n "maximum," >> $hourLog

	for(( i=1; i<=11; i++ ))
        do
		if [ $i != 10 ]
		then
			awk -F ",|M" -v x=$i '{if ($x > max) max=$x}END{printf max}' $hourLogTemp >> $hourLog
		else
                        awk -F "," -v x=$i 'NR==1 {printf $x}' $hourLogTemp >> $hourLog
		fi
		
		fun_print_sep
	done
}

fun_min(){
	echo -n "minimum," >> $hourLog

	for(( i=1; i<=11; i++ ))
	do
		if [ $i != 10 ]
                then
			awk -F ",|M" -v x=$i 'BEGIN{min=9999999}{if($x<min) min=$x}END{printf min}' $hourLogTemp >> $hourLog
		else
                        awk -F "," -v x=$i 'NR==1 {printf $x}' $hourLogTemp >> $hourLog
                fi
                
		fun_print_sep
	done
}

fun_print_sep(){
	if [ $i != 11 ]
  	then
                echo -n "," >> $hourLog
       	else
                echo "M" >> $hourLog
      	fi
}

fun_agregasi(){
	echo $metDesc > $hourLog
	fun_min
	fun_max
	fun_average
	chmod 600 $hourLog
	rm $hourLogTemp
}

metDesc="type,mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size"
path=/home/dhika/
pathLog=$path"log/"
date=$(date +"%Y%m%d")
hour=$(date +"%H")
minuteLog=$pathLog"metrics_"$date$(($hour-1))"*.log"
hourLogTemp=$pathLog"metrics_agg_"$date$(($hour-1))".tmp"
hourLog=$pathLog"metrics_agg_"$date$(($hour-1))".log"

awk 'NR%2 == 0 {print}' $minuteLog > $hourLogTemp

fun_agregasi
```
